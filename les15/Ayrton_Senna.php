<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Tibo Van Tricht">
    <meta name="keywords" content="Senna, Ayrton">
    <meta name="date" content="20180208">
    <link rel="stylesheet" href="css Senna/app2.css">
    <title>Ayrton Senna</title>
</head>

<body>
    <!-- we voegen hier helemaal bovenaan anchors toe
    waarnaar we springen als we op een item in het 
    aside menu klikken, zodat de pagina niet meer
    naar bovenspringt
    -->

    <span id="span1">Ayrton Senna</span>
    <span id="span2">Wie was Ayrton Senna?</span>
    <span id="span3">Hoe is hij zo succesvol geworden?</span>
    <span id="span4">Zijn tijd bij Mclaren Honda</span>
    <span id="span5">Zijn tijd bij Williams Renault en het fatale ongeluk</span>
    <span id="span6">Zijn Bijnaam</span>
    <span id="span7">Mclaren eert Ayrton met een supercar in 2018</span>
    <span id="span8">Kort filmpje van het ongeluk</span>

    <header>
        <?php 
        include('horizontal-nav.html');
        ?>
        <div class="banner">
            <h1>Ayrton Senna</h1>
            <figure>
                <img src="Senna/Ayrton_Senna_8_-_Cropped.jpg" alt="Ayrton Senna">
            </figure>
    </header>
    <aside>
        <nav>
            <!-- ol>(li>a)*7 -->
            <ol>
                <li><a href="#span1">Ayrton Senna</a></li>
                <li><a href="#span2">Wie was Ayrton Senna?</a></li>
                <li><a href="#span3">Hoe is hij zo succesvol geworden?</a></li>
                <li><a href="#span4">Zijn tijd bij Mclaren Honda</a></li>
                <li><a href="#span5">Zijn tijd bij Williams Renault en het fatale ongeluk</a></li>
                <li><a href="#span6">Zijn Bijnaam</a></li>
                <li><a href="#span7">Mclaren eert Ayrton met een supercar in 2018</a></li>
                <li><a href="#span8">Kort filmpje van het ongeluk</a></li>

            </ol>
        </nav>
    </aside>
    <article>
        <section id="section1">
            <h1>Ayrton Senna</h1>
            <br>
            <figure>
                <img src="Senna/Ayrton_Senna_8_-_Cropped.jpg" />
                <figcaption>Ayrton Senna</figcaption>
            </figure>
            <br>
            <ul>
                <strong>Senna </strong>
                <br>
                <br>Ayrton senna da Silva is geboren op 21 maart 1960 in São Paulo, Brazilië
                <br>Hij stierf als gevolg van een crash in de Grand Prix van San Marino in 1994.
                <br>Zijn dood zorgde voor veel opschudding in het F1-milieu, en resulteerde in een hele reeks veiligheidsmaatregelen.
                <br>Hij werd begraven in Bologna op 1 mei 1994.
            </ul>
        </section>
        <section id="section2">
            <h1>Wie was Ayrton Senna?</h1>
            <br>
            <figure>
                <img src="Senna/Flag_of_Brazil(sao paulo).jpg" />
                <figcaption>Vlag van Sao Paulo</figcaption>
            </figure>
            <ul>
                <br>Ayrton senna was een zeer succesvolle Braziliaanse autocoureur die in een tijdsbestek van tien seizoenen driemaal wereldkampioen Formule 1 werd.
                <br>Hij kwam aan de start in 161 races, waarbij hij 65 keer van poleposition startte en 41 keer won.
            </ul>
        </section>
        <section id="section3">
            <h1>Hoe is hij zo succesvol geworden?</h1>
            <br>
            <figure>
                <img src="Senna/FIA_Formula_One_World_Championship_Logo.jpg" alt="F1 logo 1984">
                <figcaption>F1 logo 1984</figcaption>
                <img src="Senna/lotus97T.jpg" alt="Lotus 97T">
                <figcaption>Lotus 97T</figcaption>
                <br>
            </figure>
            <ul>
                <strong>Hoe is hij zo Succesvol geworden?</strong>
                <br>
                <br>Ayrton Senna begon zijn racecarrière op vierjarige leeftijd in een kart.
                <br>Na deelname aan de Formule Ford 1600 en 2000-klasses, won hij in 1983 de British F3 van Martin Brundle.
                <br>In 1984 maakte hij, na getest te hebben voor Williams, de overstap naar de Formule 1 door te tekenen bij het team van Toleman.
                <br>Memorabel was de GP van Monaco, die wegens hevige regenval voortijdig werd beëindigd.
                <br>Senna veroverde de tweede plaats, achter Alain Prost, maar de algemene opvatting was dat Senna de koppositie binnen een paar rondes had kunnen opeisen.
                <br>Overigens vlagde wedstrijdleider Jacky Ickx de wedstrijd pas af na hevig aandringen van leider Prost.
                <br>Dit aandringen zou Prost aan het einde van het seizoen de titel kosten.
                <br>Door het vroegtijdig afvlaggen werden er slechts halve punten toegekend en kreeg Prost voor zijn overwinning slechts 4,5 punt in plaats van de volle negen.
                <br>De Fransman verloor de titel met slechts een half punt aan Niki Lauda.
                <br>Senna eindigde uiteindelijk in zijn eerste seizoen op de 9e plaats met 13 punten in het eindklassement.
                <br>Hij behaalde tweemaal een 6e plaats en één 7e plaats.
                <br>Hij wist driemaal op het podium te eindigen: eenmaal met een 2e plaats tijdens de Grand Prix van Monaco en tweemaal met een 3e plaats,
                <br>waarvan één tijdens de Grand Prix van Engeland en de andere tijdens de laatste Grand Prix van het seizoen 1984,
                <br>die plaatsvond in Portugal op het circuit van Estoril.
                <br>Voor 1985 verbrak Senna het contract met Toleman abrupt om over te stappen naar het team van Lotus, waar hij teamgenoot werd van de Italiaan Elio De Angelis.
                <br>In de Lotus 97T won hij zijn eerste Grand Prix: in Portugal in de regen.
                <br>Later dat jaar won hij nog de Grand Prix van België.
                <br>Senna eindigde in zijn tweede seizoen op de vierde plaats in het eindklassement met 38 punten.
                <br>Naast twee overwinningen behaalde hij tweemaal een 3e plaats: tijdens de Grand Prix van Nederland en in Italië.
                <br>Tweemaal behaalde hij een 2e plaats: eenmaal tijdens de Grand Prix van Oostenrijk en eenmaal tijdens de Grand Prix van Europa,
                <br>welke plaatsvond op het Engelse circuit van Brands Hatch.
            </ul>
        </section>
        <section id="section4">
            <h1>Zijn tijd bij Mclaren Honda</h1>
            <br>
            <figure>
                <img src="Senna/senna-inline.jpg" />
                <figcaption>De Mclaren honda wagen van Senna</figcaption>
                <br>
            </figure>
            <ul>
                <strong>Zijn tijd bij Mclaren Honda</strong>
                <br>
                <br>In 1988 verhuisde Senna naar het team van McLaren en kreeg wereldkampioen Alain Prost als teamgenoot.
                <br>Het duo won 15 van de 16 races van dat jaar en Senna veroverde zijn eerste wereldtitel.
                <br>Dieptepunt uit dit jaar was zijn crash tijdens de Grand Prix van Monaco,
                <br>waarin hij in leidende positie in de eindfase van de race de vangrail raakte.
                <br>Senna reed comfortabel aan de leiding, Prost was tweede en greep vervolgens de overwinning.
                <br>Desondanks zou Senna er toch in slagen om de Grand Prix van Monaco zes keer op zijn palmares te schrijven.
                <br>In 1989 begon de samenwerking met Alain Prost fout te lopen.
                <br>De twee coureurs ontwikkelden zowel op als naast de baan diverse conflicten,
                <br>wat resulteerde in een crash tussen de twee tijdens de laatste race: de Grand Prix van Japan.
                <br>Prost viel uit door de crash en Senna – die de race won – werd na afloop van de race gediskwalificeerd vanwege gevaarlijk rijden en het veroorzaken van de crash met Prost.
                <br>Prost behaalde de wereldtitel, en stapte in 1990 over naar het team van Ferrari.
                <br>Een jaar later, weer in Japan, won Senna de kwalificatie en werd Prost tweede.
                <br>Na de kwalificatie werd besloten om de startopstelling voor de race te veranderen.
                <br>Senna moest nu vanaf polepositie starten op het vuile gedeelte van de baan (deel met minder tractie),
                <br>terwijl Prost, die als tweede op de startopstelling stond, kon starten op de ideale lijn van het circuit.
                <br>Tijdens de start reed Prost Senna voorbij, maar in de eerste bocht nam Senna geen gas terug en kwam daardoor met Prost in aanraking.
                <br>De twee raakten van de baan en konden de race niet vervolgen.
                <br>Doordat beide geen punten wonnen tijdens deze race,
                <br>Senna meer punten had opgebouwd tijdens het seizoen dan Prost en geen andere coureur dan Prost of Senna de titel kon pakken,
                <br>werd Senna na de race uitgeroepen tot F1 wereldkampioen in 1990.
            </ul>
        </section>
        <section id="section5">
            <h1>Zijn tijd bij Williams Renault en het fatale ongeluk</h1>
            <br>
            <figure>
                <img src="Senna/Williams Renault.jpg" />
                <figcaption>De Williams renault wagen van Senna</figcaption>
                <br>
            </figure>
            <ul>
                <strong>Zijn tijd bij Williams Renault en het fatale ongeluk </strong>
                <br>
                <br>Na Senna's derde wereldtitel in 1991 werden de kwaliteiten van de auto van McLaren ingehaald door die van de auto's van andere teams,
                <br>met name Williams. Senna tekende voor 1994 bij dit team.
                <br>Senna verongelukte op zondag 1 mei 1994 tijdens de Grand Prix Formule 1 van San Marino 1994 op het circuit van Imola.
                <br>In zijn vaderland Brazilië werden drie dagen van nationale rouw afgekondigd en zijn begrafenis werd over de hele wereld live uitgezonden.
                <br>Tijdens een rechtszaak tegen leden van het Williams-team werden uitvoerig alle aspecten van het ongeluk tegen het licht gehouden.
                <br>Op het eerste gezicht dacht men dat de crash aan een afgebroken deel van de stuurstang lag, maar dit was slechts een gevolg van de crash.
                <br>In de bocht waar Senna eraf ging, (Tamburello), was het asfalt zeer oneffen; hier maakten vele coureurs zich zorgen om.
                <br>Bovendien lag aan de buitenzijde van de bocht een muur gevaarlijk dicht op het parcours.
                <br>Omdat er een rivier achter de bocht lag, kon men de afstand tussen de muur en het circuit niet vergroten.
                <br>Toen Senna door Tamburello stuurde reed hij over de oneffenheid terwijl alle andere piloten erlangs reden.
                <br>Hierdoor raakte de bodemplaat van de wagen het asfalt, waardoor alle grip verdween.
                <br>De Williams reed rechtdoor en boorde zich in de muur.
                <br>De rondes voor Senna's crash speelden zich af achter de Safety car (door een eerder ongeluk in de race),
                <br>hierdoor was de temperatuur in de banden afgenomen en daardoor was waarschijnlijk de bandenspanning van Senna's wagen te laag.
                <br>Dit heeft een kleinere bodemspeling veroorzaakt, en is wellicht de belangrijkste oorzaak van het ongeluk.
                <br>Door de klap kwam een deel van de wielophanging los, dat tegen de rechterkant van zijn helm vloog.
                <br>Dat zorgde ervoor dat zijn hoofd tegen de kopsteun botste en hem met een fatale schedelbreuk en hersenschade van het leven beroofde.
                <br>Saillant detail is dat Senna zich vaak heeft ingezet om de veiligheid van de coureurs te verbeteren.
                <br>De dag voor het ongeluk was de Oostenrijker Roland Ratzenberger op hetzelfde circuit verongelukt.
                <br>Senna droeg als eerbetoon aan Ratzenberger een Oostenrijkse vlag in zijn wagen.
                <br>Tevens was op de vrijdag (twee dagen voor het fatale ongeval) Rubens Barrichello tijdens de vrije trainingen hard van de baan gegaan waardoor Barrichello in het ziekenhuis belandde.
                <br>Na de twee doden in één weekend werd echt werk gemaakt van het lijfsbehoud van de coureur.
                <br>Een van de aanpassingen is de inmiddels geldende minimale afstand van de bodemplaat tot de grond.
                <br>Een tweede verbetering van de veiligheid is de invoering van het sinds 2003 verplicht gestelde HANS system (bescherming voor de nek).
                <br>Jean Graton schreef een Michel Vaillant-dossier over Senna: Dossier Michel Vaillant 6: "Ayrton Senna: Het heilig vuur".
                <br>Jan Segers schreef een Nederlandstalige biografie met de titel "Senna – De rechtervoet van God".
                <br>Ayrton Senna bemoeide zich in de laatste fasen van het pre-productiestadium intensief met de ontwikkeling van de Honda NSX teneinde het weggedrag te verbeteren.
                <br>Senna stond bekend om zijn hoge technische begaafdheid.
                <br>Testsessies duurden vaak veel langer dan bij andere collega’s.
                <br>Na een testsessie of kwalificatie sloot hij zich op en kwam na een kwartier met soms wel vier A4'tjes verbeteringen aan de wagen naar buiten.
                <br>De briefings waren vaak eindeloos. Als Senna besprekingen voerde met zijn ingenieurs wilde hij alles tot in de details begrijpen.
                <br>Begreep hij iets niet dan liet hij het een tweede keer van begin af aan uitleggen en indien nodig een derde en vierde keer.
                <br>Deze gedrevenheid werkte teamgenoten, waaronder Alain Prost, soms dermate op de heupen dat zij de briefings eerder dan Senna verlieten.
            </ul>
        </section>
        <section id="section6">
            <h1>Zijn Bijnaam</h1>
            <br>
            <figure>
                <img src="Senna/360px-AyrtonSennasFullFaceHelmet.jpg" />
                <figcaption>De helm van Senna</figcaption>
                <br>
            </figure>
            <ul>
                <strong>Zijn Bijnaam </strong>
                <br>
                <br>Ayrton Senna was sterk in het kwalificeren, getuige hiervan zijn de 65 "poles" die hij in zijn carrière verzamelde.
                <br>Zijn tactiek verliep steevast volgens hetzelfde stramien.
                <br>De kwalificatiesessies duurden toen nog 1 uur, op zaterdag, waarin elke wagen een ongelimiteerd aantal ronden mocht afleggen.
                <br>In het begin van de sessie zette Senna een eerste snelle ronde neer.
                <br>Daarna kwam hij de pit binnen en liet nog enkele laatste aanpassingen aan zijn wagen uitvoeren.
                <br>Een twintigtal minuten voor het einde zette hij een tweede snelle ronde neer.
                <br>Dit was vooral om te kijken of de laatste wijzigingen aan de wagen het gewenste effect hadden.
                <br>Hierna kwam hij weer binnen en was het wachten op het hoogtepunt.
                <br>Hij zorgde er steevast voor dat hij zijn laatste snelle ronde kon inzetten vlak voordat het uur verstreken was.
                <br>Op die manier was hij de laatste die een gooi kon doen naar de polepositie.
                <br>Hij richtte al zijn concentratie en wagenbeheersing op deze laatste ronde, vaak goed genoeg voor de snelste tijd.
                <br>Niemand kon deze tijd nog verbeteren, de 60 minuten durende kwalificatiesessie was immers verstreken.
                <br>Het bekendste voorbeeld van zijn kwalificatietalent is de kwalificatie voor de Grand Prix van Monaco in 1988.
                <br>Tijdens de kwalificatie was hij de snelste met een voorsprong van 0,7 seconden op de nummer 2, zijn teamgenoot Alain Prost.
                <br>Ondanks het feit dat hij eigenlijk al zeker was van de pole, poogde hij nogmaals om zijn tijd te verbeteren.
                <br>Zijn team probeerde hem tegen te houden, maar Senna weigerde zijn gereden tijd te laten staan.
                <br>Later verklaarde Senna dat hij zijn wagen van bovenaf zag en precies kon zien waar hij wat tijd liet liggen.
                <br>Deze gegevens gebruikte hij in die ene snelle ronde, die hem op 1,5 seconden voorsprong op Prost bracht, die in dezelfde wagen reed.
                <br>Dit alles zorgde ervoor dat velen hem Magic Senna begonnen te noemen.
            </ul>
        </section>
        <section id="section7">
            <h1>Mclaren eert Ayrton met een supercar in 2018</h1>
            <br>
            <figure>
                <img src="Senna/mclaren-senna-1.jpg" />
                <figcaption>De Mclaren Senna</figcaption>
            </figure>
            <br>
            <ul>
                <strong>Mclaren Senna </strong> 
                <br>Ayrton Senna won zijn drie F1-wereldkampioenschappen dankzij McLaren (met Honda). Daarom prijkt 
                <br>zijn naam op de nieuwe hypersportwagen van het merk, de auto die we tot op heden altijd kenden als 
                <br>P15. De McLaren Senna is ontwikkeld voor het circuit, maar gehomologeerd voor de openbare weg. Hij 
                <br>wordt aangedreven door een 800 pk en 800 Nm sterke 4.0-V8 biturbo die centraal achterin is 
                <br>ingebouwd. Hij weegt slechts 1.198 kilo, dus per ton heeft hij 668 pk. Die gunstige verhouding gewicht-
                <br>vermogen is het resultaat van veelvuldig gebruik van koolstofvezel voor het monocoque-chassis en de 
                <br>koetswerkpanelen. De overbrenging gebeurt via een gerobotiseerde zevenbak met dubbele koppeling. 
                <br>Daarmee zou hij “neck-snapping”* acceleraties garanderen.
                <br>
                <br>De tweezitter heeft een elektrohydraulische stuurbekrachtiging die is gemaakt om zoveel mogelijk 
                <br>feedback te geven op circuit. De ophanging met dubbele driehoeken is geoptimaliseerd voor een 
                <br>sportief rijgedrag, maar dankzij adaptieve schokdempers kan je wel kiezen tussen de standen Comfort, 
                <br>Sport en Track. De ingenieurs uit Woking hebben de stroomlijn en de motorkoeling tot in de details 
                <br>geoptimaliseerd. Maar er is ook veel aandacht besteed aan de symfonie die uit de uitlaat zingt. McLaren 
                <br>noemt het koolstofkeramische remsysteem “het meest gesofisticeerde” ooit op een van zijn straatlegale 
                <br>modellen. Om alle geweld te kunnen kanaliseren staat de Senna op Pirelli’s P Zero Trofeo R.
                <br>
                <br>Om in de hypersportwagen in te stappen, moet je de vleugeldeuren optillen. Als optie kan je in elk van 
                <br>de deuren een van de koolstofvezel panelen vervangen door een transparant element. De cockpit is zo 
                <br>functioneel mogelijk ingedeeld, ja zelfs minimalistisch. Verschillende knoppen zijn op het plafond 
                <br>aangebracht, zoals die voor het openen van de deuren en de startknop. In dit decor vol carbon kan de 
                <br>klant kiezen tussen een stoelbekleding in leer of alcantara. De onthulling is gepland voor het autosalon 
                <br>van Genève, in maart 2018. De tot 500 exemplaren beperkte productie start in het derde trimester van 
                <br>dat jaar. De Britse prijs bedraagt 750.000 pond of zo’n 860.000 euro.
            </ul>
        </section>
        <section id="section8">
            <h1>Kort filmpje van het ongeluk</h1>
            <br>
            <ul>
                <strong>Ayrton Senna death onboard camera 1994</strong>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/gcJQC-1keb4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            </ul>
        </section>
        <footer>
            <br>
            <br>
            <address>
            Samengesteld door Tibo Van Tricht
        </address>
        </footer>
    </article>
</body>
</html>
