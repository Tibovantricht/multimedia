<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/app.css" type="text/css" />
    <script type="text/javascript" src="js/clock.js"></script>
    <title>Registeren</title>
</head>

<body>
    <header>
        <?php 
        include('horizontal-nav.html');
        ?>
        <div class="banner">
            <h1>Registreren</h1>
            <div id="clock"></div>
        </div>
    </header>
    <form method="post" action="welkom.php">
        <fieldset>
            <legend>Persoonlijke gegevens</legend>
            <div>
                <label for="firstname">Voornaam</label>
                <input id="firstname" type="text" name="" maxlength="40" required placeholder="typ hier je voornaam" />
                <span>*</span>
            </div>
            <div>
                <label for="lastname">Familienaam</label>
                <input id="lastname" type="text" name="" maxlength="80" required placeholder="typ hier je familienaam" />
                <span>*</span>
            </div>
            <div>
                <label for="password">Wachtwoord</label>
                <input id="password" type="password" name="password" maxlength="40" required/>
                <span>*</span>
            </div>
            <div>
                <label for="comment">Commentaar</label>
                <textarea id="comment" maxlength="255" name="comment"></textarea>
            </div>
            <div>
                <input name="age-group" type="radio" id="child"><label for="child">Kind</label>
                <input name="age-group" type="radio" id="young-adult"><label for="young-adult">Jong volwassene</label>
                <input name="age-group" type="radio" id="adult"><label for="adult">Volwassene</label>
            </div>
            <div>
                <label for="birthday">Geboortedatum</label>
                <input id="birthday" type="date" name="birthday" />
            </div>
            <div>
                <label for="email">E-mail</label>
                <input type="email" name="email" id="email" placeholder="naam@provider.be">
            </div>
        </fieldset>
        <fieldset>
            <legend>Persoonlijke voorkeuren</legend>
            <div>
                <input type="checkbox" name="music" id="music" /><label for="music">Muziek</label>
                <input type="checkbox" name="books" id="books" /><label for="books">Boeken</label>
                <input type="checkbox" name="movies" id="movies" /><label for="movies">Films</label>
                <input type="checkbox" name="comics" id="comics" /><label for="comics">Strips</label>
            </div>
            <div>
                <label for="courses">Modules</label>
                <select id="courses" name="courses">
                <option value="1">Multimedia</option>
                <option value="20" selected>Programmeren 2</option>
                <option value="7">Databanken</option>
            </select>
            </div>
            <div>
                <label for="range">Bent u tevreden?</label>
                <input id="range" name ="range"type="range" min="-4" max="2" step="2" value="-2" />
            </div>
        </fieldset>
        <input type="submit" value="Verzenden" />
    <p>Velden met een <span>*</span> zijn verplicht!</p>
    </form>

</body>

</html>
